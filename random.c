#include "random.h"
#include <stdlib.h>


int RandLim(int limit) {
/* return a random number between 0 and limit inclusive.
 */

    int divisor = RAND_MAX/(limit+1);
    int retval;

    do {
        retval = rand() / divisor;
    } while (retval > limit);

    return retval;
}

float RandFloatMinusOneToOne()
{

    return -1+2*((float)rand())/RAND_MAX;
}
float RandFloat()
{

    return 10000*((float)rand())/RAND_MAX;
}
