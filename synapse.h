#ifndef SYNAPSE_H_INCLUDED
#define SYNAPSE_H_INCLUDED
#include "structs.h"
#include "utlist.h"
#include "uthash.h"
#include "random.h"

int SynapseWeightAdjust(full_net **net,unsigned int neuron_id, unsigned int synapse_to, float value);
int SynapseWeightAdjustRandom();
connection *SynapseStructMakeWithDefaults(); //IMPLEMENT

#endif // SYNAPSE_H_INCLUDED
