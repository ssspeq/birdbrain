#include "firinglist.h"
extern neuron *neural_net;
extern device *device_list_head;
extern neuron_ready_to_fire *fire_list_head;

int FiringListNeuronAdd(neuron **node)
{
    neuron *firing_node = *node; //Is this the correct way?
    neuron_ready_to_fire *tmp_neuron_ready_to_fire = NULL;
    debug("Found neuron that will go in firinglist, id %u\n", firing_node->id);
    tmp_neuron_ready_to_fire = malloc(sizeof(neuron_ready_to_fire));
    if(tmp_neuron_ready_to_fire == NULL)
    {
        printf("Out of memory in firing list add\n");
        exit(EXIT_FAILURE);
    }
    tmp_neuron_ready_to_fire->neuron_struct_ptr = firing_node;
    debug("Appending neuron to firinglist\n");
    DL_PREPEND(fire_list_head, tmp_neuron_ready_to_fire);
    debug("after append\n");
    return EXIT_SUCCESS;
}

int FiringListNeuronRemove(neuron **node)
{
    //This is not optimal, figure out how to do it without searching
    debug("In remove node from firinglist\n");
    neuron *delete_node = *node;
    neuron_ready_to_fire *tmp = NULL;
    DL_SEARCH_SCALAR(fire_list_head,tmp,neuron_struct_ptr,delete_node);
    if(tmp)
    {
        debug("Found node to delete\n");
        ((neuron *)tmp->neuron_struct_ptr)->external_charge = 0;
        free((neuron *)tmp);
        DL_DELETE(fire_list_head, tmp);

        return EXIT_SUCCESS;
    }
    else
    {
        debug("FAIL\n");
        return EXIT_FAILURE;
    }
}

int FiringListNeuronsClearNonFiring()
{
    neuron_ready_to_fire *neuron_to_fire = NULL;
    neuron_ready_to_fire *tmp_neuron_to_fire;
    DL_FOREACH_SAFE(fire_list_head, neuron_to_fire, tmp_neuron_to_fire)
    {
        neuron *tmp_neuron = neuron_to_fire->neuron_struct_ptr;
        bool fire = false;
        switch(tmp_neuron->activation_function)
        {
        case ACTIVATION_FUNCTION_LEVEL:
            fire = NeuronActivationFunctionLevel(&tmp_neuron);
            break;
        }
        if(fire == false || tmp_neuron->external_charge == 0)
        {
            tmp_neuron->external_charge = 0;
            FiringListNeuronRemove(&tmp_neuron);
        }
    }
    return EXIT_SUCCESS;
}

void FiringListNeuronsFire()
{
    neuron_ready_to_fire *neuron_to_fire = NULL;
    neuron_ready_to_fire *tmp_neuron_to_fire;
    DL_FOREACH_SAFE(fire_list_head, neuron_to_fire, tmp_neuron_to_fire)
    {
        neuron *tmp_neuron;
        tmp_neuron = neuron_to_fire->neuron_struct_ptr;
        NeuronFire(&tmp_neuron);
    }
    //Now check to see what neurons should remain in the list, i.e. that have activated
}

unsigned int FiringListNeuronsCount()
{
    unsigned int count;
    neuron_ready_to_fire *tmp = NULL;
    DL_COUNT(fire_list_head, tmp, count);
    return count;
}

void FiringListPrint()
{
    printf("In firing list:\n");
    neuron_ready_to_fire *tmp_neuron_ready_to_fire = NULL;
    DL_FOREACH(fire_list_head, tmp_neuron_ready_to_fire)
    {
        neuron *tmp_neuron = NULL;
        tmp_neuron = tmp_neuron_ready_to_fire->neuron_struct_ptr;
        printf("Neuron id %u with charge level %f\n", tmp_neuron->id, tmp_neuron->external_charge);
    }
}
