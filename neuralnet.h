#ifndef NEURALNET_H_INCLUDED
#define NEURALNET_H_INCLUDED
#include "uthash.h"
#include "utlist.h"
#include <stdbool.h>
#include "constants.h"
#include "structs.h"
#include "firinglist.h"
#include "device.h"
#include <inttypes.h>
#include <math.h>
//#include "dbg.h"

int NeuralNetUnloadAndFreeEntire(full_net **net);
full_net *NeuralNetStructMakeWithDefaults();
full_net *NeuralNetCreateAndLoadRandom(unsigned int num_neurons, unsigned int num_synapses_per_neuron, int neuron_type);
neuron *NeuralNetNeuronAddToUnloaded(full_net **net, unsigned int neuron_id, int activation_function, int neuron_type);
neuron *NeuralNetNeuronAddToLoaded(full_net **net, int activation_function, int neuron_type);
int NeuralNetNeuronRemoveAndFree(full_net **net, unsigned int node_id);
int NeuralNetInternalLogicCreateRandom();
void NeuralNetCycleTimeMeasure(full_net **net);
void NeuralNetCycleFull(full_net **net, unsigned int cycles);
void NeuralNetReset(full_net **net);
void NeuralNetStatusPrint(full_net **net);
void NeuralNetMutate(); //IMPLEMENT
#endif // NEURALNET_H_INCLUDED
