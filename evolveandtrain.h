#ifndef EVOLVEANDTRAIN_H_INCLUDED
#define EVOLVEANDTRAIN_H_INCLUDED
#include <signal.h>
#include "signalhandler.h"

void Evolve(unsigned int num_neurons, unsigned int num_synapses_per_neuron, int neuron_type);
int FreeNeuralNets(full_net **network_list);
void FitnessCheckNeuralNet(full_net **net);
int FitnessCheckGeneration(full_net **network_list);
void RunNeuralNet(full_net **net, unsigned int cycles);
full_net *MakeInitialPopulation();
int MakeNewGeneration(full_net **network_list);
int ScoreSort(full_net *a, full_net *b);
void TryNeuralNet(full_net **net, unsigned int net_input, int repetitions, int patience_level);
void PresentCorrectAnswerNeuralNet(full_net **net, unsigned int correct_answer);

#endif // EVOLVEANDTRAIN_H_INCLUDED
