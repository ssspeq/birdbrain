#ifndef NEURON_H_INCLUDED
#define NEURON_H_INCLUDED
#include "structs.h"
#include "utlist.h"
#include "uthash.h"
#include "constants.h"
#define NDEBUG
#include "dbg.h"
#include "random.h"
#include "firinglist.h"
#include <inttypes.h>
#include "neuralnet.h"
#include "calculations.h"
#include "synapse.h"
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))


void NeuronSynapsesAddRandom(neuron **node, unsigned int num_neurons, int num_synapses);
int NeuronSynapseAddRandom(); //MISSINGOR FORGOTTEN?
int NeuronSynapseAdd(neuron **node, float weight, unsigned int to, int type);
int NeuronSynapseRemoveRandom();
int NeuronSynapseRemove(full_net **net, unsigned int node_id, unsigned int to_neuron);
int NeuronSynapseReverseListAdd(full_net **net, unsigned int neuron_id, unsigned int from_id);
void NeuronFireRandom(); //IMPLEMENT
void NeuronFire(full_net **net, neuron **node);
void NeuronFireReverse(full_net **net, neuron **node);
int NeuronFireSynapse(full_net **net, connection **synapse, neuron **node);
int NeuronFireInject(full_net **net, unsigned int neuron_id);
int NeuronUpdate(full_net **net, neuron **node);
unsigned int NeuronGetRandom();
bool NeuronActivationFunctionLevel(full_net **net, neuron** node);
bool NeuronActivationFunctionReverse(full_net **net, neuron** node);
neuron *NeuronPointerGet(full_net **net, unsigned int neuron_id);
neuron *NeuronStructMakeWithDefaults(full_net **net);
int NeuronSetCloseToMeCoordinates(full_net **net, neuron **own_node, neuron **other_node);
neuron *NeuronGetClosestToMe(full_net **net, neuron **node);
#endif // NEURON_H_INCLUDED
