#ifndef DEVICE_H_INCLUDED
#define DEVICE_H_INCLUDED
#include "utlist.h"
#include "uthash.h"
#include "structs.h"
#include "constants.h"
#include "neuron.h"

int DeviceCreateAndLoad(full_net **net, unsigned int width, unsigned int device_class);
int DeviceRemoveAndFree(full_net **net, unsigned int device_id);
int DeviceRemoveAllAndFree(full_net **net);
unsigned int DeviceDataGet(full_net **net, unsigned int device_id);
int DeviceDataSet(full_net **net,unsigned int device_id, unsigned int data);
int DeviceDataReset(full_net **net,unsigned int device_id);
#endif // DEVICE_H_INCLUDED
