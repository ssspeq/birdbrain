#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fileoperations.h"
#include "neuralnet.h"
#include "utlist.h"
#include "uthash.h"
#include "time.h"
#include <stdbool.h>
#include <ctype.h>
#include "device.h"
#include "synapse.h"
#include "neuron.h"
#define NDEBUG
#include "dbg.h"
#include "signalhandler.h"
#include "evolveandtrain.h"
#include "run.h"

void waitFor (unsigned int secs);
bool IsValidInt(const char *str);
void GetInputAndCreateNeuralNet();
bool keeprunning;

int main(int argc, char *argv[])

{
    keeprunning = true;
    //signal(SIGINT, sigintHandler);

    if (argc == 1)
    {
        fprintf(stderr, "%s -help if you don't know what you are doing\n", argv[0]);
        exit(1);
    }

    srand ( time(NULL) );
    if(strcmp(argv[1], "-c") == 0)
    {
        GetInputAndCreateNeuralNet();

    }

    if(strcmp(argv[1], "-i") == 0)
    {
        if(argc == 3)
        {
            printf("Examining neuralnet in %s, wait...\n", argv[2]);
            full_net *this_net = NULL;
            this_net = ReadNetworkFromFile(argv[2]);
            NeuralNetStatusPrint(&this_net);
            NeuralNetUnloadAndFreeEntire(&this_net);
        }
        else
        {
            printf("Wrong number of arguments, you forgot something\n");
        }
    }

    if(strcmp(argv[1], "-l") == 0)
    {
        if(argc == 4)
        {
            printf("Loading neuralnet from %s\n", argv[2]);
            full_net *this_net = ReadNetworkFromFile(argv[2]);
            RunNeuralNet(&this_net, atoi(argv[3]));
        }

        else
        {
            printf("Wrong number of arguments, you forgot something\n");
        }
    }

    if(strcmp(argv[1], "-e") == 0)
    {
        if(argc == 2)
        {
            printf("Nothing here yet.\n");
            //neural_net = ReadNetworkFromFile(argv[2]);
        }

        else
        {
            printf("Wrong number of arguments, you forgot something\n");
        }
    }
    if(strcmp(argv[1], "-test") == 0)
    {
        if(argc == 2)
        {
            printf("Test\n");
        }
    }
    if(strcmp(argv[1], "-help") == 0)
    {
        printf("Usage:\n");
        printf("-c:    creates a new neuralnet and stores it to file filename\n");
        printf("-l X Y:loads neuralnet from X and runs it Y cycles\n");
        printf("-i:    filename prints info about neural net in filename\n");
        printf("-e:    Starts evolution with randomness (will fix user input)\n");
        printf("-test: Run internal tests (NOT IMPLEMENTED YET)\n");

    }

    return 0;
}



void GetInputAndCreateNeuralNet()
{
    char networkfilename[100];
    printf("Filename for the new shiny neural net?\n");
    fgets(networkfilename, 100, stdin);
    networkfilename[strlen(networkfilename)-1] = '\0';
    unsigned int number_of_neurons = 0;
    printf("Number of neurons? (Above 100k and things might get slow and memory hungry)\n");
    scanf("%u", &number_of_neurons);
    int number_of_synapses_per_neuron = 0;
    printf("Number of synapses per neuron?\n");
    scanf("%d", &number_of_synapses_per_neuron);
    if(number_of_neurons < number_of_synapses_per_neuron)
    {
        printf("You cant have more synapses than neurons. Thats silly.\n");
        exit(EXIT_FAILURE);
    }
    printf("Creating network, hold on...\n");
    full_net *this_net = NeuralNetCreateAndLoadRandom(number_of_neurons, number_of_synapses_per_neuron, NEURON_TYPE_CHARGE);
    if(this_net == NULL)
    {
        printf("Something went wrong in creating network.\n");
        exit(EXIT_FAILURE);
    }
    printf("Writing network to file %s\n",networkfilename);
    DumpNetworkToFile(&this_net, networkfilename);
    NeuralNetUnloadAndFreeEntire(&this_net);
    printf("Done! Enjoy your new neural net.\n");
}

bool IsValidInt(const char *str)
{
    // Handle negative numbers.
    //
    if (*str == '-')
        ++str;

    // Handle empty string or just "-".
    //
    if (!*str)
        return false;

    // Check for non-digit chars in the rest of the stirng.
    //
    while (*str)
    {
        if (!isdigit(*str))
            return false;
        else
            ++str;
    }

    return true;
}

void waitFor (unsigned int secs)
{
    double retTime;

    retTime = time(0) + secs;     // Get finishing time.
    while (time(0) < retTime);    // Loop until it arrives.
}
