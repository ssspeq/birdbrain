#ifndef BITSANDLOGIC_H_INCLUDED
#define BITSANDLOGIC_H_INCLUDED
#include <inttypes.h>
#include <stdlib.h>
#include "constants.h"

u_int8_t BitGet(u_int32_t decimal, int N);
u_int8_t Logic(u_int8_t a, u_int8_t b, u_int8_t logic_function);

#endif // BITSANDLOGIC_H_INCLUDED
