#include "calculations.h"

float CalculateDistance3D(float x0, float y0, float z0, float x1, float y1, float z1)
// TODO (per#1#): TEST THAT THIS IS RETURNING THE CORRECT DISTANCE
{
    //Can this return negative distance if variables are entered in the wrong order?
    //Lets make sure it cant.
    float deltaX = 0;
    float deltaY = 0;
    float deltaZ = 0;
    if(x1 > x0)
    {
        deltaX = x1 - x0;
    }
    else
    {
        deltaX = x0 - x1;
    }
    if(y1 > y0)
    {
        deltaY = y1 - y0;
    }
    else
    {
        deltaY = y0 - y1;
    }
    if(z1 > z0)
    {
        deltaZ = z1 - z0;
    }
    else
    {
        deltaZ = z0 - z1;
    }


    return sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);
}
