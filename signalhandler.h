#ifndef SIGNALHANDLER_H_INCLUDED
#define SIGNALHANDLER_H_INCLUDED

#include <stdio.h>
#include <signal.h>
#include "neuralnet.h"
#include <stdbool.h>
bool keeprunning;
void sigintHandler(int sig_num);

#endif // SIGNALHANDLER_H_INCLUDED
