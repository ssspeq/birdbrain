#ifndef FIRINGLIST_H_INCLUDED
#define FIRINGLIST_H_INCLUDED
#include "structs.h"
#include "utlist.h"
#include "uthash.h"
//#include "dbg.h"
#include "neuron.h"

int FiringListNeuronAdd(neuron **node);
int FiringListNeuronRemove(neuron **node);
int FiringListClear(); //IMPLEMENT
int FiringListNeuronsClearNonFiring();
void FiringListNeuronsFire();
unsigned int FiringListNeuronsCount();
void FiringListPrint();

#endif // FIRINGLIST_H_INCLUDED
