#include "synapse.h"

int SynapseWeightAdjust(full_net **net, unsigned int neuron_id, unsigned int synapse_to, float value)
{
    full_net *this_net = *net;
    neuron *node = NULL;
    connection *synapse;
    HASH_FIND_INT(this_net->neural_net, &neuron_id, node);
    if(node == NULL)
    {
        return EXIT_FAILURE;
    }
    DL_SEARCH_SCALAR(node->synapse_list_forward_head, synapse, to, synapse_to);
    if(synapse != NULL)
    {
        synapse->weight += value;
        if(synapse->weight > 1)
        {
            synapse->weight = 1;
        }
        if(synapse->weight < -1)
        {
            synapse->weight = -1;
        }
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}

int SynapseWeightAdjustRandom(full_net **net)
{
// TODO (per#1#): FIX data types, unsigned int!

    //Pick random neuron
    full_net *this_net = *net;
    neuron *targetnode = NULL;
    unsigned int num_neurons_total = HASH_COUNT(this_net->neural_net);
    if(num_neurons_total == 0)
    {
        return EXIT_FAILURE;
    }
    unsigned int random_number;
    while(1)
    {
        random_number = RandLim(num_neurons_total);
        HASH_FIND_INT(this_net->neural_net, &random_number, targetnode);
        if(targetnode != NULL)
        {
            break;
        }
    }
    unsigned int count;
    connection *synapse = NULL;
    DL_COUNT(targetnode->synapse_list_forward_head,synapse,count);
    if(count == 0)
    {
        return EXIT_FAILURE;
    }
    random_number = RandLim(count-1);
    synapse = NULL;
    unsigned int i = 0;
    DL_FOREACH(targetnode->synapse_list_forward_head, synapse)
    {

        if(random_number == i)
        {
            float value = 0;
            value = RandFloatMinusOneToOne();
            synapse->weight += value;
            if(synapse->weight > 1)
            {
                synapse->weight = 1;
            }
            if(synapse->weight < -1)
            {
                synapse->weight = -1;
            }
            return EXIT_SUCCESS;
        }
        else
        {
            i++;
        }
    }
    return EXIT_FAILURE;
}
