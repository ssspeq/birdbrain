#include "evolveandtrain.h"

void Evolve(unsigned int num_neurons, unsigned int num_synapses_per_neuron, int neuron_type)
{

}

full_net *MakeInitialPopulation(unsigned int num_neurons, unsigned int num_synapses_per_neuron, int neuron_type)
{
    full_net *my_networks = NULL;

    int i;
    for(i=0; i<10; i++)
    {
        full_net *current_net = NeuralNetCreateAndLoadRandom
        (num_neurons, num_synapses_per_neuron, neuron_type);
        if(current_net == NULL)
        {
            printf("Bad pointer from create network.\n");
            exit(EXIT_FAILURE);
        }
        current_net->id = i;
        current_net->correct_answers = 0;
        char filename[100];
        snprintf(filename, sizeof(filename), "%d", i);
        strcpy(current_net->filename, filename);
        HASH_ADD_INT(my_networks, id, current_net);
    }
    return my_networks;
}

int MakeNewGeneration(full_net **network_list)
{
    return EXIT_SUCCESS;
}

int ScoreSort(full_net *a, full_net *b)
{
    return (a->correct_answers - b->correct_answers);
}

int FitnessCheckGeneration(full_net **network_list)
{
    return EXIT_SUCCESS;
}

int FreeNeuralNets(full_net **network_list)
{
    full_net *my_networks = *network_list;
    full_net *current_net = NULL;
    full_net *current_net_tmp = NULL;
    HASH_ITER(hh, my_networks, current_net, current_net_tmp)
    {
        HASH_DEL(my_networks, current_net);
        NeuralNetUnloadAndFreeEntire(&current_net);

    }
    free(my_networks);

    return EXIT_SUCCESS;
}

void FitnessCheckNeuralNet(full_net **net)
{

}
void TryNeuralNet(full_net **net, unsigned int net_input, int repetitions, int patience_level)
{

}

void PresentCorrectAnswerNeuralNet(full_net **net, unsigned int correct_answer)
{


}


