#include <stdio.h>
#include <stdlib.h>
#include "neuralnet.h"
#include "random.h"
#include <time.h>
#include <unistd.h>

void NeuralNetCycleFull(full_net **net, unsigned int cycles)
{
    full_net *this_net = *net;
    int nodes_firing = 0;
    neuron *node = NULL;
    unsigned int cycle_count = 0;
    while(cycle_count <= cycles)
    {
    nodes_firing = 0;
    for(node=this_net->neural_net; node != NULL; node=node->hh.next)
    {
        if(NeuronActivationFunctionLevel(&this_net, &node) == true)
            {
                NeuronFire(&this_net, &node);
                nodes_firing++;
            }
    }
    for(node=this_net->neural_net; node != NULL; node=node->hh.next)
    {
        NeuronUpdate(&this_net, &node);
    }
    cycle_count++;
    printf("Neurons firing :%d\n", nodes_firing);
    }
}

full_net *NeuralNetStructMakeWithDefaults()
{
    full_net *this_net = malloc(sizeof(full_net));
    if(this_net == NULL)
        {
            printf("Out of memory in create random neural net\n");
            exit(EXIT_FAILURE);
        }

    this_net->neural_net = NULL;
    this_net->device_list_head = NULL;
    this_net->internal_charge_threshold = 0.5;
    this_net->internal_charge_threshold_self_fire = 1;
    this_net->internal_charge_gain_per_cycle = 0.0;
    this_net->internal_charge_leak_when_above_resting_potential = 0;
    this_net->internal_charge_after_fire = -0.5;
    this_net->internal_resting_potential = 0;
    this_net->use_temporal_summation = false;
    strcpy(this_net->filename, "");
    this_net->correct_answers = 0;
    this_net->generation = 0;
    return this_net;

}

neuron *NeuralNetNeuronAddToLoaded(full_net **net, int activation_function, int neuron_type)
{
    full_net *this_net = *net;
    //Lets just randomize the number of synapses, but no more than 1% of number of neurons
    neuron *new_neuron = NULL;
    new_neuron = NeuronStructMakeWithDefaults(&this_net);
    if(new_neuron == NULL)
    {
        return NULL;
    }
    new_neuron->id = HASH_COUNT(this_net->neural_net)+1;
    new_neuron->activation_function = activation_function;
    new_neuron->neuron_type = neuron_type;
    //The neuron/node id is now the number of neurons in the net hopefully
    //if uniqueness if properly enforced
    unsigned int num_synapses = new_neuron->id * 0.02;
    if(num_synapses == 0)//If total of neurons is less than 10, nummber of synapses will be <1
    {
        num_synapses = 1;
    }
    NeuronSynapsesAddRandom(&new_neuron, new_neuron->id, num_synapses);
    HASH_ADD_INT( this_net->neural_net, id, new_neuron);
    return new_neuron;

}

void NeuralNetCycleTimeMeasure(full_net **net)
{

}

int NeuralNetUnloadAndFreeEntire(full_net **net)
{
    full_net *this_net = *net;
    if(this_net == NULL)
    {
        printf("Bad pointer handed to unload neural net. It cant be blamed for this.\n");
        exit(EXIT_FAILURE);
    }
    DeviceRemoveAllAndFree(&this_net);
    neuron *node = NULL;
    neuron *node_tmp = NULL;
    connection *synapse = NULL;
    connection *synapse_tmp = NULL;
    reverse_connection *r_connection = NULL;
    reverse_connection *r_connection_tmp = NULL;
    HASH_ITER(hh, this_net->neural_net, node, node_tmp)
    {
        if(node != NULL)
        {
            DL_FOREACH_SAFE(node->synapse_list_reverse_head, r_connection, r_connection_tmp)
            {
                //DL_DELETE() Do we need to do delete here? Probably not?
                free(r_connection);
            }
            DL_FOREACH_SAFE(node->synapse_list_forward_head, synapse, synapse_tmp)
            {
                free(synapse);
            }
        }
        HASH_DEL(this_net->neural_net, node);
        free(node);
    }
    free(this_net->device_list_head);
    free(this_net->neural_net);
    free(this_net);
    return EXIT_SUCCESS;
}

int NeuralNetNeuronRemoveAndFree(full_net **net, unsigned int node_id)
{
    full_net *this_net = *net;
    //Removes neuron from net and wipes out all the synapses to it, I hope :)
    neuron *delnode;
    connection *delconnection = NULL; //Need this pointer to search DL
    connection *tmp;
    HASH_FIND_INT(this_net->neural_net, &node_id, delnode);
    if(delnode != NULL && delnode->neuron_type != NEURON_TYPE_DEVICE_INPUT
            && delnode->neuron_type != NEURON_TYPE_DEVICE_OUTPUT)
    {
        DL_FOREACH_SAFE(delnode->synapse_list_forward_head, delconnection, tmp)
        {
            NeuronSynapseRemove(&this_net, delnode->id, delconnection->to);
        }
        //Now connections should be gone.
        HASH_DEL(this_net->neural_net, delnode);
        free(delnode);
        //Now we need the delete and free all connections there were in the net to
        //the deleted neuron AND ALSO REVERSE CONNECTIONS THAT MAY HAVE BEEN ADDED
        neuron *tmp_neuron = NULL;
        neuron *current_neuron = NULL;
        HASH_ITER(hh, this_net->neural_net, current_neuron, tmp_neuron)
        {
            delconnection = NULL;
            DL_SEARCH_SCALAR(current_neuron->synapse_list_forward_head,delconnection,to,node_id);
            if(delconnection != NULL)
            {
                NeuronSynapseRemove(&this_net,current_neuron->id, delconnection->to);
            }
        }
        return EXIT_SUCCESS;
    }
    else
    {
        return EXIT_FAILURE;
    }
}

neuron *NeuralNetNeuronAddToUnloaded(full_net **net, unsigned int neuron_id, int activation_function, int neuron_type)
{
    full_net *this_net = *net;
    neuron *new_neuron = NeuronStructMakeWithDefaults(&this_net);
    if(new_neuron == NULL)
    {
        return NULL;
    }
    new_neuron->id = neuron_id;
    new_neuron->activation_function = activation_function;
    new_neuron->neuron_type = neuron_type;
    HASH_ADD_INT( this_net->neural_net, id, new_neuron);
    //printf("There are total %d number of neurons\n", HASH_COUNT(this_net->neural_net));
    return new_neuron;

}

void NeuralNetStatusPrint(full_net **net)
{
    full_net *this_net = *net;
    unsigned int total_number_of_synapses = 0;
    unsigned int total_number_of_electrical_synapses = 0;
    //unsigned int total_number_of_chemical_synapses = 0;
    unsigned int total_number_of_chemical_synapses_a = 0;
    unsigned int total_number_of_chemical_synapses_b = 0;
    unsigned int total_number_of_chemical_synapses_c = 0;
    unsigned int total_number_of_chemical_synapses_d = 0;
    unsigned int total_number_of_chemical_synapses_e = 0;

    unsigned int total_number_of_output_devices = 0;
    unsigned int total_number_of_input_devices = 0;
    neuron *tmp_neuron = NULL;
    for(tmp_neuron=this_net->neural_net; tmp_neuron != NULL; tmp_neuron=tmp_neuron->hh.next)
    {
        connection *tmp_synapse_tmp = NULL;
        DL_FOREACH(tmp_neuron->synapse_list_forward_head, tmp_synapse_tmp)
        {
            total_number_of_synapses ++;
            switch(tmp_synapse_tmp->type)
            {
            case CHEMICAL_TYPE_A:
                total_number_of_chemical_synapses_a++;
                break;
            case CHEMICAL_TYPE_B:
                total_number_of_chemical_synapses_b++;
                break;
            case CHEMICAL_TYPE_C:
                total_number_of_chemical_synapses_c++;
                break;
            case CHEMICAL_TYPE_D:
                total_number_of_chemical_synapses_d++;
                break;
            case CHEMICAL_TYPE_E:
                total_number_of_chemical_synapses_e++;
                break;
            case ELECTRICAL_SYNAPSE:
                total_number_of_electrical_synapses++;
                break;
            }
        }
    }
    printf("This neuralnet has %u neurons and %u synapses.\n", HASH_COUNT(this_net->neural_net),
           total_number_of_synapses);
    printf("There are the following synapse types:\n");
    printf("Chemical type a synapses: %d\n", total_number_of_chemical_synapses_a);
    printf("Chemical type b synapses: %d\n", total_number_of_chemical_synapses_b);
    printf("Chemical type c synapses: %d\n", total_number_of_chemical_synapses_c);
    printf("Chemical type d synapses: %d\n", total_number_of_chemical_synapses_d);
    printf("Chemical type e synapses: %d\n", total_number_of_chemical_synapses_e);
    printf("Electrical synapses     : %d\n", total_number_of_electrical_synapses);

    device *tmp_device = NULL;
    DL_FOREACH(this_net->device_list_head, tmp_device)
    {
        if(tmp_device != NULL && tmp_device->device_class == DEVICE_TYPE_INPUT)
        {
            total_number_of_input_devices++;
        }
        if(tmp_device != NULL && tmp_device->device_class == DEVICE_TYPE_OUTPUT)
        {
            total_number_of_output_devices++;
        }
    }
    printf("It has %u input device(s) and %u output device(s), total %u device(s).\n", total_number_of_input_devices,
           total_number_of_output_devices, (total_number_of_input_devices+total_number_of_output_devices));
}

full_net *NeuralNetCreateAndLoadRandom(unsigned int num_neurons, unsigned int num_synapses_per_neuron, int neuron_type)
{
// TODO (per#1#): Make sure to check that the number of neurons is enough for devices.
    full_net *this_net = NeuralNetStructMakeWithDefaults();
    if(this_net == NULL)
    {
        printf("Memory error in create random neural net\n");
        exit(EXIT_FAILURE);
    }
    neuron *node = NULL;
    int activation_function = 0;
    unsigned int neuron_id = 0;
    unsigned int i;
    for(i = 0; i < num_neurons; i++)
    {
        neuron_id = i;
        node = NeuralNetNeuronAddToUnloaded(&this_net, neuron_id, activation_function, neuron_type);
        NeuronSynapsesAddRandom(&node, num_neurons, num_synapses_per_neuron);

    }
    //MAKE DEFAULT DEVICES NOW PLEASE
    DeviceCreateAndLoad(&this_net, 8, DEVICE_TYPE_OUTPUT);
    DeviceCreateAndLoad(&this_net, 8, DEVICE_TYPE_INPUT);
    DeviceCreateAndLoad(&this_net, 8, DEVICE_TYPE_INPUT);
    //NeuralNetStatusPrint(&this_net);
    return this_net;
}
