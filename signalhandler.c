#include "signalhandler.h"
// Signal Handler for SIGINT
void sigintHandler(int sig_num)
{
    printf("\n Got control C, cleaning up. \n");
    keeprunning = false;
    fflush(stdout);
}
