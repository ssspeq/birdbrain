#ifndef FILEOPERATIONS_H_INCLUDED
#define FILEOPERATIONS_H_INCLUDED

#include "neuralnet.h"
#include "utlist.h"
#define MAX_LINE_SIZE 1000

full_net *ReadNetworkFromFile(char *file_name);
int DumpNetworkToFile(full_net **net, char *file_name);
device* ReadDevicesFromFile(char *file_name); //IMPLEMENT
int DumpDevicesToFile(char *file_name); //IMPLEMENT
FILE *OpenFile(char *file_name, char *mode);

#endif // FILEOPERATIONS_H_INCLUDED
