#include "bitsandlogic.h"

u_int8_t BitGet(u_int32_t decimal, int N)
{

// Shifting the 1 for N-1 bits
    u_int32_t constant = 1 << (N);

// if the bit is set, return 1
    if( decimal & constant )
    {
        return 1;
    }

// If the bit is not set, return 0
    return 0;
}

u_int8_t Logic(u_int8_t a, u_int8_t b, u_int8_t logic_function)
{
    u_int8_t result = 0;
    switch(logic_function)
    {
    case GATE_NAND:
        result = ~(a & b);
        break;

    case GATE_XNOR:
        result = ~(a^b);
        break;
    case GATE_AND:
        result = (a & b);
        break;
    case GATE_OR:
        result = (a | b);
        break;
    case GATE_NOR:
        result = ~(a|b);
        break;
    case GATE_XOR:
        result = (a^b);
        break;
    }
    return result;
}
