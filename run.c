#include "run.h"

void RunNeuralNet(full_net **net, unsigned int cycles)
{
    full_net *this_net = *net;

//    unsigned int net_output = 0;
    unsigned int net_input = 255;
    DeviceDataSet(&this_net, 1, net_input);
    NeuralNetCycleFull(&this_net, cycles);
    printf("Device 0 is %d\n", DeviceDataGet(&this_net,0));
    NeuralNetUnloadAndFreeEntire(&this_net);
}
