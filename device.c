#include "device.h"

int DeviceCreateAndLoad(full_net **net, unsigned int width, unsigned int device_class)
{
    //Create a network device so it can communicate should it want to.
    full_net *this_net = *net;
    if(HASH_COUNT(this_net->neural_net) < width)
    {
        printf("Not enought neurons for device width.\n");
        exit(EXIT_FAILURE);
    }
    device *new_device = malloc(sizeof(device));
    if(new_device == NULL)
    {
        printf("Out of memory, cant create device.\n");
        exit(EXIT_FAILURE);
    }

    //Check for next available device id, do it easy and stupidly
    unsigned int device_id = 0;
    device *tmp_device = NULL;
    DL_COUNT(this_net->device_list_head, tmp_device, device_id);
    new_device->id = device_id;
    new_device->device_class = device_class;
    new_device->width = width;
    new_device->data = 0;


    //Now lets hook it up to some neurons
    unsigned int number_of_neurons_registered = 0;
    neuron *node = NULL;

    for(node=this_net->neural_net; node != NULL; node=node->hh.next)
    {
        if(node->neuron_type != NEURON_TYPE_DEVICE_INPUT
        && number_of_neurons_registered < width
        && node->neuron_type != NEURON_TYPE_DEVICE_OUTPUT)
        {
            node->neuron_type = device_class;
            node->device_id = device_id;
            node->bit = number_of_neurons_registered;
            number_of_neurons_registered++;

            if(device_class == DEVICE_TYPE_INPUT)
                {
                    //There is no point for the neuron to have chem connections.
//                    connection *current_synapse = NULL;
//                    connection *tmp_synapse = NULL;
//                    DL_FOREACH_SAFE(node->synapse_list_forward_head, current_synapse, tmp_synapse)
//                    {
//                        if(current_synapse != NULL && current_synapse->type==SYNAPSE_TYPE_CHEMICAL)
//                        {
//                            NeuronSynapseRemove(&this_net, node->id, current_synapse->to);
//                            //NeuronSynapseAdd()
//                        }
//                    }
                }
        }
        if(number_of_neurons_registered == width)
        {
            DL_APPEND(this_net->device_list_head, new_device);
            return EXIT_SUCCESS;
        }

    }
    return EXIT_FAILURE;
}

int DeviceRemoveAndFree(full_net **net, unsigned int device_id)
{
    full_net *this_net = *net;
    device *tmp_device = NULL;
    DL_SEARCH_SCALAR(this_net->device_list_head, tmp_device, id, device_id);

    if(tmp_device != NULL)
    {
        //Now, we need to check every neuron and remove its connection to this device
        neuron *node = NULL;
        for(node=this_net->neural_net; node != NULL; node=node->hh.next)
        {
            if(node != NULL && node->device_id == device_id)
            {
                node->device_id = 0;
                node->neuron_type = NEURON_TYPE_CHARGE; //SHOULD NOT MATTER
                node->bit = 0;
            }
        }
        DL_DELETE(this_net->device_list_head, tmp_device);
        free(tmp_device);
        return EXIT_SUCCESS;
    }
    else
    {
        return EXIT_FAILURE;
    }
}

int DeviceRemoveAllAndFree(full_net **net)
{
    full_net *this_net = *net;
    device *del_device = NULL;
    device *tmp_device = NULL;
    DL_FOREACH_SAFE(this_net->device_list_head, del_device, tmp_device)
    {
        if(del_device != NULL)
        {
            DeviceRemoveAndFree(&this_net, del_device->id);
        }
    }
    return EXIT_SUCCESS;
}

unsigned int DeviceDataGet(full_net **net, unsigned int device_id)
{
    full_net *this_net = *net;
    device *tmp_device = NULL;
    DL_SEARCH_SCALAR(this_net->device_list_head, tmp_device, id, device_id);
    {
        if(tmp_device != NULL)
        {
            return tmp_device->data;
        }
        else
        {
            printf("Bad pointer in device data get\n");
            exit(EXIT_FAILURE);
        }
    }
}

int DeviceDataSet(full_net **net, unsigned int device_id, unsigned int data)
// TODO (per#1#): Fix this so there is no need to search for "bit neurons", it takes a lot of time, and time is critical for this.
{
    full_net *this_net = *net;
    device *tmp_device = NULL;
    DL_SEARCH_SCALAR(this_net->device_list_head, tmp_device, id, device_id);

    if(tmp_device != NULL && tmp_device->device_class == DEVICE_TYPE_INPUT)
    {
        //Set the proper "bits" by igniting the neurons that corresponds.
        //(1 << bit) & byte //RETURNS ZERO IF BIT NOT SET
        unsigned int width = tmp_device->width;
        neuron *node = NULL;
        unsigned int i;
        for(i=0; i<width; i++)
        {
            for(node=this_net->neural_net; node != NULL; node=node->hh.next)
            {
                if(node->device_id == device_id && node->bit == i)
                {
                    if(((1 << i) & data))
                    {
                            node->internal_charge = 1;
                            //node->marked_for_firing = true;
                            //NeuronFire(&node);

                    }
                }
            }

        }
        tmp_device->data = data;
        return EXIT_SUCCESS;
    }
    else
    {
        printf("Bad pointer in device data set.\n");
        exit(EXIT_FAILURE);
    }

}

int DeviceDataReset(full_net **net, unsigned int device_id)
{
    full_net *this_net = *net;
    device *tmp_device = NULL;
    DL_SEARCH_SCALAR(this_net->device_list_head, tmp_device, id, device_id);
    {
        if(tmp_device != NULL)
        {
            tmp_device->data = 0;
            return EXIT_SUCCESS;
        }
        else
        {
            printf("Bad pointer in device data reset.\n");
            exit(EXIT_FAILURE);
        }
    }
}
