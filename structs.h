#ifndef STRUCTS_H_INCLUDED
#define STRUCTS_H_INCLUDED
#include "stdbool.h"
#include "uthash.h"
#include "constants.h"

//Electrical synapses only adds positive charge. They have lower propagation time.
//Can be altered in strength depending on prev activity.
//Chemical synapses can add both positive and negative charge.

typedef struct connection
{
    unsigned int to;
    unsigned int from;
    float weight;
    int type;
    int time_to_be_active;
    bool activated;
    int usage_level;
    bool passed_on_to_forward_neuron;
    struct connection *next;
    struct connection *prev;
} connection;

typedef struct reverse_connection
{
    connection *connection_struct_ptr;
    struct reverse_connection *next;
    struct reverse_connection *prev;

} reverse_connection;

typedef struct neuron
{
    unsigned int id;
    float internal_charge;
    int activation_function;
    float x;
    float y;
    float z;
    int neuron_type;
    float internal_threshold;
    int chemical_status;
    int history;
    bool marked_for_firing;
    bool got_reverse_signal;
    unsigned int device_id;
    unsigned int bit;

    connection *synapse_list_forward_head;
    reverse_connection *synapse_list_reverse_head;
    UT_hash_handle hh;
} neuron;

typedef struct device
{
    unsigned int id;
    unsigned int device_class;
    unsigned int data;
    unsigned int width;
    struct device *next;
    struct device *prev;
} device;


typedef struct full_net
{
    unsigned int id;
    char filename[100];
    unsigned int generation;
    neuron *neural_net;
    device *device_list_head;
    float internal_charge_threshold;
    float internal_charge_gain_per_cycle;
    float internal_charge_leak_when_above_resting_potential;
    float internal_charge_after_fire;
    float internal_charge_threshold_self_fire;
    float internal_resting_potential;
    bool use_temporal_summation;
    int correct_answers;
    unsigned int average_neurons_firing;
    float average_tries_before_learning;
    UT_hash_handle hh;
} full_net;

#endif // STRUCTS_H_INCLUDED
