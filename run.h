#ifndef RUN_H_INCLUDED
#define RUN_H_INCLUDED
#include "neuralnet.h"

void RunNeuralNet(full_net **net, unsigned int cycles);

#endif // RUN_H_INCLUDED
