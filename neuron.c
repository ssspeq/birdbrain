#include "neuron.h"

int NeuronUpdate(full_net **net, neuron **node)
{
    full_net *this_net = *net;
    neuron *current_node = *node;
    connection *synapse = NULL;
    reverse_connection *r_connection = NULL;

    //Sum up incoming charges if the node is not an input neuron
    if(current_node->neuron_type != NEURON_TYPE_DEVICE_INPUT)
    {
        DL_FOREACH(current_node->synapse_list_reverse_head, r_connection)
        {
            synapse = r_connection->connection_struct_ptr;
            if(synapse->activated == true)
            {

                switch(synapse->type)
                {
                case ELECTRICAL_SYNAPSE:
                //printf("Firing electrical\n");
                    current_node->internal_charge += (1*synapse->weight);
                    break;
                case CHEMICAL_TYPE_A:
                //printf("Injecting chem a\n");
                    current_node->internal_charge += (CHEMICAL_TYPE_A_EFFECT);
                    break;
                case CHEMICAL_TYPE_B:
                //printf("Injecting chem b\n");
                    current_node->internal_charge += (CHEMICAL_TYPE_B_EFFECT);
                    break;
                }
            }
        }
    }

    if(current_node->internal_charge < this_net->internal_resting_potential)
    {
        current_node->internal_charge +=this_net->internal_charge_gain_per_cycle;
    }
    else if(current_node->internal_charge > this_net->internal_resting_potential)
    {
        current_node->internal_charge -= this_net->internal_charge_leak_when_above_resting_potential;
    }

    connection *current_synapse = NULL;

    DL_FOREACH(current_node->synapse_list_forward_head, current_synapse)
    {

        if(current_synapse != NULL)
        {
            if(current_synapse->time_to_be_active > 0)
            {

                current_synapse->time_to_be_active--;
            }
            else if(current_synapse->time_to_be_active == 0)
            {
                //printf("Deactivating synapse\n");
                current_synapse->activated = false;
            }
        }
        if(current_synapse->usage_level >= USAGE_LEVEL_LOW)
        {
            current_synapse->usage_level--;
        }
    }
    return EXIT_SUCCESS;
}

void NeuronSynapsesAddRandom(neuron **node, unsigned int num_neurons, int num_synapses)
{
    int y;
    unsigned int random_to_id;
    float random_weight;
    int random_synapse_type = 0;
    int decimal_random_synapse_type = 0;
    neuron *targetnode = *node;
    for(y = 0; y < num_synapses; y++)
    {
        //Make random connection to some neuron
        //Make sure never to add several synapses to the same neuron
        // ALSO MUST NEVER CREATE CONNECTION TO ITSELF
        connection *synapse_search = NULL;
        while(1)
        {
            //printf("In while loop\n");
            random_synapse_type = 0;
            random_to_id = RandLim(num_neurons-1);
            //Decide synapse type
            decimal_random_synapse_type = RandLim(2);
            random_synapse_type = random_synapse_type | (1 << decimal_random_synapse_type);

            DL_SEARCH_SCALAR(targetnode->synapse_list_forward_head,synapse_search,to,random_to_id);
            //There are no previous synapses so approve this
            if(synapse_search == NULL && targetnode->id != random_to_id)
            {
                break;
            }
            //There already is some tope of synapse to this node

            else if(synapse_search != NULL
                    && synapse_search->type == CHEMICAL_TYPE_A
                    && random_synapse_type != CHEMICAL_TYPE_A)
            {
                break;
            }
            else if(synapse_search != NULL
                    && synapse_search->type == CHEMICAL_TYPE_B
                    && random_synapse_type != CHEMICAL_TYPE_B)
            {
                break;
            }
            else if(synapse_search != NULL
                    && synapse_search->type == CHEMICAL_TYPE_C
                    && random_synapse_type != CHEMICAL_TYPE_C)
            {
                break;
            }
            else if(synapse_search != NULL
                    && synapse_search->type == CHEMICAL_TYPE_D
                    && random_synapse_type != CHEMICAL_TYPE_D)
            {
                break;
            }
            else if(synapse_search != NULL
                    && synapse_search->type == ELECTRICAL_SYNAPSE
                    && random_synapse_type != ELECTRICAL_SYNAPSE)
            {
                break;
            }
        }
        random_weight = RandFloatMinusOneToOne();
        NeuronSynapseAdd(&targetnode, random_weight, random_to_id, random_synapse_type);
    }
}

int NeuronSynapseAdd(neuron **node, float weight, unsigned int to, int type)
{

    neuron *targetnode = *node;
    connection *synapse = NULL;
    connection *synapse_search = NULL;
    synapse = malloc(sizeof(connection));
    if(synapse == NULL)
    {
        debug("Out of memory\n");
        exit(EXIT_FAILURE);
    }
    synapse->to = to;
    synapse->from = targetnode->id;
    synapse->weight = weight;
    synapse->type = type;
    synapse->time_to_be_active = 0;
    synapse->activated = false;
    synapse->usage_level = 0;
    synapse->passed_on_to_forward_neuron = false;
    synapse->type = type;
    //Does this neuron already have this connection?
    DL_FOREACH(targetnode->synapse_list_forward_head,synapse_search)
    {
        //Not sure this will work
// TODO (per#1#): Check if double synapse check works, dont think so.

        if(synapse_search == synapse || synapse_search == NULL)
        {
            printf("Double connection error, why is this not fixed?\n");
            exit(EXIT_FAILURE);
        }
    }
    DL_APPEND(targetnode->synapse_list_forward_head, synapse);
    return EXIT_SUCCESS;
}

int NeuronSynapseRemoveRandom(full_net **net)
{
    //Pick random neuron
    full_net *this_net = *net;
    neuron *targetnode = NULL;
    unsigned int num_neurons_total = HASH_COUNT(this_net->neural_net);
    if(num_neurons_total == 0)
    {
        return EXIT_FAILURE;
    }
    unsigned int random_number;
    while(1)
    {
        random_number = RandLim(num_neurons_total);
        HASH_FIND_INT(this_net->neural_net, &random_number, targetnode);
        if(targetnode != NULL)
        {
            break;
        }
    }
    unsigned int count = 0;
    connection *synapse_search = NULL;
    DL_COUNT(targetnode->synapse_list_forward_head,synapse_search,count);
    if(count == 0)
    {
        return EXIT_FAILURE;
    }
    random_number = RandLim(count-1);
    synapse_search = NULL;
    unsigned int i = 0;
    DL_FOREACH(targetnode->synapse_list_forward_head, synapse_search)
    {
        if(random_number == i)
        {
            NeuronSynapseRemove(&this_net, targetnode->id, synapse_search->to);
            return EXIT_SUCCESS;
        }
        else
        {
            i++;
        }
    }
    return EXIT_FAILURE;
}

int NeuronSynapseRemove(full_net **net, unsigned int node_id, unsigned int to_neuron)
{
// TODO (per#1#): Check that this is working properly. I am not sure.
    full_net *this_net = *net;
    neuron *current_neuron = NULL;
    connection *delconnection = NULL;
    reverse_connection *r_connection = NULL;
    HASH_FIND_INT(this_net->neural_net, &node_id, current_neuron);
    if(current_neuron != NULL)
    {
        DL_SEARCH_SCALAR(current_neuron->synapse_list_forward_head,delconnection,to,to_neuron);
        if(delconnection != NULL)
        {
            neuron *remote_neuron = NeuronPointerGet(&this_net,delconnection->to);
            if(remote_neuron == NULL)
            {
                printf("Bad pointer in remove synapse\n");
                exit(EXIT_FAILURE);
            }
            DL_DELETE(current_neuron->synapse_list_forward_head, delconnection);
            DL_SEARCH_SCALAR(remote_neuron->synapse_list_reverse_head,
                             r_connection, connection_struct_ptr, delconnection);
            if(r_connection)
            {
                DL_DELETE(remote_neuron->synapse_list_reverse_head, r_connection);
                free(r_connection);
            }
            free(delconnection);

            return EXIT_SUCCESS;
        }
    }
    return EXIT_FAILURE;
}

int NeuronSynapseReverseListAdd(full_net **net, unsigned int neuron_id, unsigned int from_id)
{

    full_net *this_net = *net;
    neuron *remote_node = NeuronPointerGet(&this_net,from_id);
    if(remote_node == NULL)
    {
        printf("Bad pointer in reverse list add\n");
        exit(EXIT_FAILURE);
    }

    bool should_be_added = true;
    neuron *neuron_to_be_updated = NULL;
    connection *synapse = NULL;
    reverse_connection *r_connection = NULL;
    reverse_connection *r_connection_new = NULL;

    neuron_to_be_updated = NeuronPointerGet(&this_net, neuron_id);
    if(neuron_to_be_updated == NULL)
    {
        printf("Bad pointer in reverse list add, neuron type\n");
        exit(EXIT_FAILURE);
    }
    DL_FOREACH(neuron_to_be_updated->synapse_list_reverse_head, r_connection)
    {
        if(((connection *)r_connection->connection_struct_ptr)->from == from_id)
        {
            should_be_added = false;
        }
    }
    if(should_be_added == true)
    {

        neuron *remote_node = NeuronPointerGet(&this_net, from_id);
        if(remote_node == NULL)
        {
            printf("Bad pointer in reverse list add\n");
            exit(EXIT_FAILURE);
        }
        DL_FOREACH(remote_node->synapse_list_forward_head, synapse)
        {
            if(synapse->to == neuron_id)
            {
                r_connection_new = malloc(sizeof(reverse_connection));
                if(r_connection_new == NULL)
                {
                    printf("Bad pointer in reverse list add\n");
                    exit(EXIT_FAILURE);
                }
                r_connection_new->connection_struct_ptr = synapse;
                DL_APPEND(neuron_to_be_updated->synapse_list_reverse_head, r_connection_new);
                return EXIT_SUCCESS;
            }
        }
    }
    return EXIT_FAILURE;
}

void NeuronFireReverse(full_net **net, neuron **node)
{
    full_net *this_net = *net;
    neuron *current_node = *node;
    neuron *target_node = NULL;
    connection *synapse = NULL;
    reverse_connection *r_connection = NULL;
    current_node->got_reverse_signal = false;
    printf("Neuron reverse fire\n");
    DL_FOREACH(current_node->synapse_list_reverse_head, r_connection)
    {
        synapse = ((connection *)r_connection->connection_struct_ptr);
        //Now lets adjust this synapse
        if(synapse->usage_level <= USAGE_LEVEL_LOW)
        {
            SynapseWeightAdjust(&this_net, synapse->from, synapse->to, -0.2);
        }
        else if(synapse->usage_level >= USAGE_LEVEL_MEDIUM)
        {
            SynapseWeightAdjust(&this_net, synapse->from, synapse->to, 0.1);
        }
        else if(synapse->usage_level >= USAGE_LEVEL_HIGH)
        {
            SynapseWeightAdjust(&this_net, synapse->from, synapse->to, 0.2);
        }

        target_node = NeuronPointerGet(&this_net,synapse->from);
        if(target_node != NULL && target_node->got_reverse_signal == false)
        {
            target_node->got_reverse_signal = true;
        }
    }
}

void NeuronFire(full_net **net, neuron **node)
{
    full_net *this_net = *net;
    neuron *firing_node = *node;
    connection *synapse = NULL;
    firing_node->internal_charge = this_net->internal_charge_after_fire;

    if(firing_node == NULL)
    {
        printf("bad pointer in neuronfire\n");
        exit(EXIT_FAILURE);
    }

    debug("Will fire node %d with charge %f\n", firing_node->id,firing_node->internal_charge);
    //Check if this is a neuron belonging to a device so we can hijack it
    if(firing_node->neuron_type == NEURON_TYPE_DEVICE_OUTPUT)
    {
        device *target_device = NULL;
        DL_SEARCH_SCALAR(this_net->device_list_head, target_device, id, firing_node->device_id);
        if(target_device != NULL && target_device->device_class == DEVICE_TYPE_OUTPUT)
        {
            //Register the bit value that this node represents
            target_device->data = target_device->data | (1 << firing_node->bit);
            return;
        }
    }
    //Now fire all the synapses of this neuron
    DL_FOREACH(firing_node->synapse_list_forward_head, synapse)
    {
        NeuronFireSynapse(&this_net, &synapse, &firing_node);
    }

}

int NeuronFireSynapse(full_net **net, connection **synapse, neuron **node)
{
    full_net *this_net = *net;
    neuron *target_neuron = NULL;
    unsigned int target_neuron_id = 0;
    connection *firing_synapse = *synapse;
    target_neuron = NeuronPointerGet(&this_net, firing_synapse->to);
    if(target_neuron == NULL)
    {
        printf("Bad pointer fire\n");
        return EXIT_FAILURE;
    }
    if(firing_synapse->passed_on_to_forward_neuron == false)
    {
        NeuronSynapseReverseListAdd(&this_net, target_neuron_id, firing_synapse->from);
    }
    firing_synapse->passed_on_to_forward_neuron = true;
    //Update usage level for this synapse
    if(firing_synapse->usage_level <= USAGE_LEVEL_HIGH)
    {
        firing_synapse->usage_level++;
    }
    firing_synapse->activated = true;
    switch(firing_synapse->type)
    {
    case CHEMICAL_TYPE_A:
        firing_synapse->time_to_be_active = CHEMICAL_TYPE_A_DURATION;
        break;
    case CHEMICAL_TYPE_B:
        firing_synapse->time_to_be_active = CHEMICAL_TYPE_B_DURATION;
        break;
    case CHEMICAL_TYPE_C:
        firing_synapse->time_to_be_active = CHEMICAL_TYPE_C_DURATION;
        break;
    case CHEMICAL_TYPE_D:
        firing_synapse->time_to_be_active = CHEMICAL_TYPE_D_DURATION;
        break;
    case ELECTRICAL_SYNAPSE:
        firing_synapse->time_to_be_active = ELECTRICAL_SYNAPSE_DURATION;
        break;
    }

    return EXIT_SUCCESS;
}

int NeuronFireInject(full_net **net, unsigned int neuron_id)
{
    full_net *this_net = *net;
    neuron *node = NULL;

    HASH_FIND_INT( this_net->neural_net, &neuron_id, node );
    if(node == NULL)
    {
        return EXIT_FAILURE;
    }
    if(node->neuron_type == NEURON_TYPE_CHARGE)
    {
        node->internal_charge = 1;
    }
    else if(node->neuron_type == NEURON_TYPE_DEVICE_INPUT)
    {
        node->internal_charge = 1;
    }
    return EXIT_SUCCESS;
}

bool NeuronActivationFunctionLevel(full_net **net, neuron** node)
{
    full_net *this_net = *net;
    neuron *current_neuron = *node;

    if(current_neuron->internal_charge > this_net->internal_charge_threshold)
    {
        debug("fire!\n");
        return true;
    }
    else
    {
        return false;
    }
}

bool NeuronActivationFunctionReverse(full_net **net, neuron** node)
{
    full_net *this_net = *net;
    neuron *current_neuron = *node;
    if(current_neuron->internal_charge >= this_net->internal_charge_threshold
            && current_neuron->got_reverse_signal == true)
    {
        printf("FIRE REVERSE!\n");
        return true;
    }
    else if(current_neuron->internal_charge >= this_net->internal_charge_threshold_self_fire)
    {
        int fire_or_not = RandLim(2);
        if(fire_or_not == 1)
        {
            printf("FIRE REVERSE\n");
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

neuron *NeuronPointerGet(full_net **net, unsigned int neuron_id)
{
    full_net *this_net = *net;
    neuron *node = NULL;
    HASH_FIND_INT( this_net->neural_net, &neuron_id, node );
    return node;
}

neuron *NeuronStructMakeWithDefaults(full_net **net)
{
    full_net *this_net = *net;
    neuron *new_neuron = NULL;
    new_neuron = malloc(sizeof(neuron));
    if(new_neuron == NULL)
    {
        return NULL;
    }
    new_neuron->device_id = 0;
    new_neuron->bit = 0;
    new_neuron->internal_threshold = 0;
    new_neuron->synapse_list_forward_head = NULL;
    new_neuron->synapse_list_reverse_head = NULL;
    new_neuron->internal_charge = this_net->internal_resting_potential;
    new_neuron->chemical_status = 0;
    new_neuron->neuron_type = NEURON_TYPE_CHARGE;
    new_neuron->marked_for_firing = false;
    new_neuron->got_reverse_signal = false;
    new_neuron->history = 0;
    //Make random position for this neuron
    //The odds of collision are miniscule so skip the check. I know...

    new_neuron->x = RandFloat();
    new_neuron->y = RandFloat();
    new_neuron->z = RandFloat();


    return new_neuron;
}

neuron *NeuronGetClosestToMe(full_net **net, neuron **node)
{
    full_net *this_net = *net;
    neuron *node1 = *node;
    neuron *node2 = NULL;
    float shortest_distance = 0;
    float current_distance = 0;
    unsigned int nearest_node = 0;

    for(node2=this_net->neural_net; node2 != NULL; node2=node2->hh.next)
    {
        current_distance = CalculateDistance3D(node1->x,node1->y,node1->z,node2->x,node2->y,node2->z);
        if(current_distance < shortest_distance)
        {
            shortest_distance = current_distance;
            nearest_node = node2->id;
        }
    }
    node2 = NULL;
    HASH_FIND_INT(this_net->neural_net, &nearest_node, node2);
    return node2;
}

int NeuronSetCloseToMeCoordinates(full_net **net, neuron **own_node, neuron **other_node)
{
    full_net *this_net = *net;
    neuron *my_node = *own_node;
    neuron *neighbour_node = *other_node;
    neuron *iter_node = NULL;
    int direction = RandLim(2);
    int offset = 0.1;
    bool coordinate_ok = false;
    while(coordinate_ok == false)
    {
        coordinate_ok = true;
        for(iter_node=this_net->neural_net; iter_node != NULL; iter_node=iter_node->hh.next)
        {
            if(direction == DIRECTION_X && iter_node->x == my_node->x+offset)
            {
                coordinate_ok = false;
            }
            if(direction == DIRECTION_Y && iter_node->y == my_node->y+offset)
            {
                coordinate_ok = false;
            }
            if(direction == DIRECTION_Z && iter_node->z == my_node->z+offset)
            {
                coordinate_ok = false;
            }
        }
        if(coordinate_ok == false)
        {
            offset++;
        }

    }
    if(direction == DIRECTION_X)
    {
        neighbour_node->x = my_node->x+offset;
        neighbour_node->y = my_node->y;
        neighbour_node->z = my_node->z;
    }
    else if(direction == DIRECTION_Y)
    {
        neighbour_node->x = my_node->x;
        neighbour_node->y = my_node->y+offset;
        neighbour_node->z = my_node->z;
    }
    else if(direction == DIRECTION_Z)
    {
        neighbour_node->x = my_node->x;
        neighbour_node->y = my_node->y;
        neighbour_node->z = my_node->z+offset;
    }
    return EXIT_SUCCESS;

}

unsigned int NeuronGetRandom(full_net **net)
{
    full_net *this_net = *net;
    unsigned int random_id = 0;
    neuron *random_neuron = NULL;
    while(random_neuron == NULL)
    {
        random_id = RandLim(HASH_COUNT(this_net->neural_net)-1);
        HASH_FIND_INT(this_net->neural_net, &random_id, random_neuron);
    }
    return random_neuron->id;
}
