#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "fileoperations.h"
#include "dbg.h"

full_net *ReadNetworkFromFile(char *file_name)
{
    full_net *this_net = NeuralNetStructMakeWithDefaults();
    neuron *node = NULL;
    FILE *input_file;
    char *next_field;  // ptr to the next field extracted from the current line
    char line[MAX_LINE_SIZE+1]; // ptr to the current input line
    input_file = OpenFile(file_name, "r");
    if(input_file == NULL)
    {
        return NULL;
    }
    connection *synapse = NULL;

    // read the file one line at a time.
    while (1)
    {
        fgets(line, MAX_LINE_SIZE, input_file); // read the line from input_file

        // if we've reached the end of the input_file, break out of the loop
        if (feof(input_file))
            break;


        if (strstr(line, "//") != NULL)
        {
            //printf("Ignoring comment\n");
            continue;
        }
        if(strstr(line, "<n>") != NULL)
        {

            fgets(line, MAX_LINE_SIZE, input_file);
            //printf("Creating a new neuron\n");
            node = NeuronStructMakeWithDefaults(&this_net);
            if(node == NULL){exit(EXIT_FAILURE);}
            next_field = strtok(line, " \n");
            int y;
            for(y = 0; y < 9; y++)
            {
                if(next_field == NULL)
                {
                    debug("File error, craping pants\n");
                    fclose(input_file);
                    return NULL;
                }
                switch(y)
                {
                case 0 :
                    //printf("id: %s\n", next_field );
                    node->id = atoi (next_field);
                    break;
                case 1 :
                    //printf("neuron type: %s\n", next_field );
                    node->neuron_type = atoi (next_field);
                    break;
                case 2 :
                    //printf("x: %s\n", next_field );
                    node->x = atof (next_field);
                    break;
                case 3 :
                    //printf("y: %s\n", next_field );
                    node->y = atof(next_field);
                    break;
                case 4 :
                    //printf("z: %s\n", next_field );
                    node->z = atof(next_field);
                    break;
                case 5 :
                    //printf("activation function: %s\n", next_field );
                    node->activation_function = atoi (next_field);
                    break;
                case 6 :
                    //printf("Internal threshold: %f\n", next_field );
                    node->internal_threshold = atof (next_field);
                    break;
                case 7 :
                    //printf("Neuron device id: %u\n", next_field );
                    node->device_id = atoi(next_field);
                    break;
                case 8 :
                    //printf("Bit: %u\n", next_field );
                    node->bit = atoi(next_field);
                    break;

                }
                next_field = strtok(NULL, " \n");
            }
            //printf("Appending neuron\n");
            HASH_ADD_INT( this_net->neural_net, id, node);
            while (1)
            {
                fgets(line, MAX_LINE_SIZE, input_file);
                if(strstr(line, "</n>") == NULL)
                {
                    synapse = malloc(sizeof(connection));

                    if(synapse == NULL)
                    {
                        debug("Out of memory, waaah");
                        fclose(input_file);
                        return NULL;
                    }

                    next_field = strtok(line, " \n");
                    int y;
                    for(y = 0; y < 4; y++)
                    {
                        if(next_field == NULL)
                        {
                            debug("File error, exiting\n");
                            fclose(input_file);
                            return NULL;
                        }
                        switch(y)
                        {
                        case 0 :
                            //Find neuron that should have this connection
                            //HASH_FIND_INT( neural_net, &neuron_id, node );
                            //printf("Neuron in hash:%d\n", node->id);
                            if(node == NULL)
                            {
                                debug("Error, invalid data\n");
                                fclose(input_file);
                                exit(EXIT_FAILURE);
                            }
                            synapse->to = atoi(next_field);
                            DL_APPEND(node->synapse_list_forward_head, synapse);

                            break;
                        case 1 :
                            synapse->from = atoi(next_field);
                            //printf("From: %s\n", next_field );

                        case 2 :
                            synapse->weight = atof(next_field);
                            //printf("Weight: %s\n", next_field );
                            break;
                        case 3 :
                            synapse->type = atoi(next_field);
                            //printf("Type: %s\n", next_field );

                            break;

                            }
                        next_field = strtok(NULL, " \n");
                    }

                }
                else
                {
                    break;
                }
            synapse->activated = false;
            synapse->time_to_be_active = 0;
            synapse->passed_on_to_forward_neuron = false;
            synapse->usage_level = 0;
            }

        }

        if(strstr(line, "<d>") != NULL) //We found a device!
        {
            unsigned int new_device_id = 0;
            unsigned int new_device_width = 0;
            unsigned int new_device_class = 0;
            fgets(line, MAX_LINE_SIZE, input_file);
            //printf("Creating a new device\n");

            next_field = strtok(line, " \n");
            int y;
            for(y = 0; y < 3; y++)
            {
                if(next_field == NULL)
                {
                    debug("File error, craping pants\n");
                    fclose(input_file);
                    return NULL;
                }
                switch(y)
                {
                case 0 :
                    //printf("Device id: %u\n", next_field );
                    new_device_id = atoi (next_field);
                    break;
                case 1 :
                    //printf("Device class: %u\n", next_field );
                    new_device_class = atoi (next_field);
                    break;
                case 2 :
                    //printf("Device width: %s\n", next_field );
                    new_device_width = atoi(next_field);
                    break;
                }
                next_field = strtok(NULL, " \n");
            }
            device *new_device = NULL;
            new_device = malloc(sizeof(device));

            if(new_device == NULL)
            {
                debug("Out of memory, waaah");
                fclose(input_file);
                return NULL;
            }
            new_device->id = new_device_id;
            new_device->device_class = new_device_class;
            new_device->width = new_device_width;
            new_device->data = 0;
            //Now add device to list
            DL_APPEND(this_net->device_list_head, new_device);
        }


    }
    fclose(input_file);
    synapse = NULL;
    node = NULL;
    for(node=this_net->neural_net; node != NULL; node=node->hh.next)
    {
        DL_FOREACH(node->synapse_list_forward_head, synapse)
        {
            if(synapse->passed_on_to_forward_neuron == false)
                {
                    NeuronSynapseReverseListAdd(&this_net, synapse->to, synapse->from);
                }
                synapse->passed_on_to_forward_neuron = true;
         }
    }
    return this_net;
}


int DumpNetworkToFile(full_net **net, char *file_name)
{
    full_net *this_net = *net;
    debug("Dumping net to file\n");
    neuron *node = NULL;
    connection *synapse = NULL;
    FILE *input_file;
    input_file = OpenFile(file_name, "w");
    if(input_file == NULL)
    {
        debug("Error opening dump file");
        exit(EXIT_FAILURE);
    }
    for(node=this_net->neural_net; node != NULL; node=node->hh.next)
    {
        fprintf(input_file,"<n>\n");
        fprintf(input_file, "%u %u %f %f %f %d %f %u %u\n", node->id, node->neuron_type,
                node->x, node->y, node->z, node->activation_function, node->internal_threshold,
                node->device_id, node->bit);
        DL_FOREACH(node->synapse_list_forward_head, synapse)
        {
            fprintf(input_file, "%u %u %f %d\n",
                    synapse->to, synapse->from, synapse->weight, synapse->type);
        }
        fprintf(input_file,"</n>\n");
    }
    //Now dump devices
    device *tmp_device = NULL;
    DL_FOREACH(this_net->device_list_head, tmp_device)
    {
        fprintf(input_file,"<d>\n");
        fprintf(input_file, "%u %u %u\n", tmp_device->id, tmp_device->device_class, tmp_device->width);
        fprintf(input_file,"</d>\n");
    }
    fclose(input_file);
    return EXIT_SUCCESS;
}

FILE *OpenFile(char *file_name, char *mode)
{
    FILE *input_file;
    // open the input file and make sure that it is opened properly
    input_file = fopen(file_name, mode);
    if (input_file == NULL)
    {
        fprintf(stderr, "Error opening '%s': %s\n",
                file_name, strerror(errno));

    }
    return input_file;
}

